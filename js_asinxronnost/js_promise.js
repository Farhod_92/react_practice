//***************1***********************
//console.log('Request data..');

// setTimeout(()=>{
//     console.log('Preparing data');

//     const backendData = {
//         server: 'aws',
//         port: 2000,
//         status: 'working'
//     }

//     setTimeout(()=>{
//         backendData.modified = true
//       console.log('Data received', backendData);
//     }, 2000)

// }, 2000)


//*************** 2 ***********************
// const p = new Promise((resolve, reject)=>{
//    console.log('Request data..');
//    setTimeout(()=>{
//         console.log('Preparing data..');
//         const backendData = {
//         server: 'aws',
//         port: 8080,
//         status: 'working'
//     }
//     resolve(backendData)
//    }, 2000) 
// })

// p.then((backendData)=>{
//     return new Promise((resolve, reject)=>{
//         setTimeout(()=>{
//             backendData.modified = true
//             reject(backendData)
//         }, 2000)
//     })

//     // p2.then((backendData)=>{
//     //     console.log('MODIFIED DATA', backendData);
//     // })
// })
// .then((backendData)=>{
//     console.log('MODIFIED DATA', backendData);
//     backendData.fromPromise = true
//     return backendData
// }).then((backendData)=>{
//     console.log("backendData", backendData);
// }).catch(err => console.log("Error:" , err))
// .finally(()=>{
//     console.log('Finally');
// })

//*************** 3  * ********************** 
const sleep = (ms) => {
    return new Promise(resolve => setTimeout(()=>{
        resolve()
    }, ms)
    )
}

// sleep(2000).then(()=>console.log('log after 2 sec'))
// sleep(3000).then(()=>console.log('log after 3 sec'))

Promise.all([sleep(2000), sleep(4000)]).then(()=>{
    console.log('All promises done');
})


Promise.race([sleep(2000), sleep(4000)]).then(()=>{
    console.log('RACE promises done');
})