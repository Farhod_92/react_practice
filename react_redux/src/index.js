import React from 'react';
import ReactDOM from 'react-dom';
import store from './store/store';

import {Provider} from "react-redux";
import App from './components/App';
import ViewName from './components/ViewName'

ReactDOM.render(
    <Provider store={store}>
      <React.StrictMode>
        <App />
        <ViewName/>
      </React.StrictMode>
    </Provider>,
  document.getElementById('root')
);
