
const initialState = {
    name:'Botir'
}

const nameReducer = (state = initialState, action) => {
    switch (action.type){
        case 'ADD_NAME':
            return {
                ...state,
                ...action.payload
            }
        case 'ADD_AGE':
            return {
            //
        }
        default: return state
    }
}

export default nameReducer