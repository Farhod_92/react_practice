import * as React from 'react';
import { useState, useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import { Button, Container, Paper } from '@material-ui/core';


export default function Student() {
  const paperStyle={padding:'50px 20px', width:600, margin:'20px auto'}
  const [address, setAddress] = useState('')
  const [name, setName] = useState('')
  const [students, setStudents]=useState([])

  const btnSubmitOnClick = (e) => {
    e.preventDefault()
    const student = {name, address}
    console.log(student);
    fetch("http://localhost:8080/student/add",{
      method:'POST',
      headers:{'Content-type':'application/json'},
      body:JSON.stringify(student)
    }).then(()=>{
      console.log('New student added');
    })
  }

  useEffect(()=>{
    fetch("http://localhost:8080/student/getAll")
    .then(res=>res.json())
    .then((result)=>{
      setStudents(result);
    }
  )
  },[])

  return (
    <Container>
      <Paper elevation={3} style={paperStyle}>
        <h1 style={{color:'blue', textDecoration:'underlined'}}>Add student</h1>
        <form>
          <TextField id="outlined-basic" label="name" variant="outlined" fullWidth value={name} onChange={(e)=>setName(e.target.value)} />
          <TextField id="outlined-basic" label="address" variant="outlined" fullWidth value={address} onChange={(e)=>setAddress(e.target.value)} />
          <Button variant="outlined"  color="secondary" onClick={btnSubmitOnClick}>Submit</Button>
        </form>
      </Paper>

      <h1>Students</h1>
      <Paper elevation={3} style={paperStyle}>
        {students.map(student=>(
          <Paper elevation={6} style={{margin:"10px",padding:"15px", textAlign:"left"}} key={student.id}>
            Id:{student.id}<br/>
            Name:{student.name}<br/>
            Address:{student.address}
          </Paper>
        ))
        }
      </Paper>

    </Container>
  );
}
