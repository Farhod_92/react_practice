import * as React from 'react';
import { AppBar, Box, Toolbar, Typography, IconButton } from "@material-ui/core"
import MenuIcon from '@material-ui/icons/Menu';

export default function ButtonAppBar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="medium"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6">
           Spring boot React application
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
