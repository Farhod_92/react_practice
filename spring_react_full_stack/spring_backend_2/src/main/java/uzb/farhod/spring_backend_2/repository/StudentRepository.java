package uzb.farhod.spring_backend_2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uzb.farhod.spring_backend_2.entity.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student,Integer> {
}