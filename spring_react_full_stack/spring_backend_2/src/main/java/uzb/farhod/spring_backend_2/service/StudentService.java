package uzb.farhod.spring_backend_2.service;

import uzb.farhod.spring_backend_2.entity.Student;

import java.util.List;

public interface StudentService {
    public Student saveStudent(Student student);
    public List<Student> getAllStudents();
}