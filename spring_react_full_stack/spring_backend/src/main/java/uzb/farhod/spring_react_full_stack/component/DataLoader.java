package uzb.farhod.spring_react_full_stack.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import uzb.farhod.spring_react_full_stack.model.User;
import uzb.farhod.spring_react_full_stack.repository.UserRepository;

@Component
public class DataLoader implements CommandLineRunner {

    private final UserRepository userRepository;

    public DataLoader(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        userRepository.save(new User("Farhod", "Hotamov", "f@mail.ru"));
        userRepository.save(new User("Tom", "raider", "f@mail.ru"));
        userRepository.save(new User("Smith", "Cruz", "f@mail.ru"));
        userRepository.save(new User("Tara", "Jackson", "f@mail.ru"));
    }
}
