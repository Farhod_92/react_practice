package uzb.farhod.spring_react_full_stack.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.spring_react_full_stack.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
}
