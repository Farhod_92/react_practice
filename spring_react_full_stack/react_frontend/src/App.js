import UserComponent from './component/UserComponent';

function App() {
  return (
    <div >
      <header className="App-header">
        <UserComponent/>
      </header>
    </div>
  );
}

export default App;
