import React, {useState, useEffect} from "react";

function randomNumber(){
  return Math.trunc(Math.random() * 20)
}

function App() {
 const [user, setUser] = useState("posts")
 const [data, setData] = useState([])
 const [position, setPosition] = useState({
   x:0,
   y:0
 })

 useEffect(()=>{
   console.log('render');
   loadJson()
 },[user])

 useEffect(()=>{
   window.addEventListener('mousemove',(event)=>{
      setPosition({x:event.clientX, y:event.clientY})
   })
 }, [])

 function loadJson(){
  fetch('https://jsonplaceholder.typicode.com/'+user)
  .then(response => response.json())
  .then(json => setData(json))
 }

  return (
    <div className='container'>
      <p>Users: {user} developer</p>
      <div className="btn-group">
          <button className="success" onClick={()=>setUser('todos')}>Todos</button>
          <button className="danger" onClick={()=>setUser('posts')}>Postst</button>
          <button className="secondary" onClick={()=>setUser('users')}>Users</button>
      </div>

      <pre>
        {JSON.stringify(position, null, 2)}
      </pre>

    </div>
  );
}

export default App;
