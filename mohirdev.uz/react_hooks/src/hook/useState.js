import React, {useState} from "react";

function randomNumber(){
  return Math.trunc(Math.random() * 20)
}

function App() {
  const [number, setNumber] = useState(randomNumber)

  const [state, setState] = useState({
    text: "Number",
    date: Date.now()
  })

  function inc(){
    setNumber((prev)=>{
      return prev+1
    })
    setNumber((prev)=>prev+1)
  }

  function dec(){
    setNumber(number-1)
    //ishlamaydi chunki asinxron ishlaydi
    setNumber(number-1)
  }

  return (
    <div className='container'>
       <div className='btn-group'>
          <p>Number: {number}</p>
          <button className='success' onClick={inc}>Add</button>
          <button className='danger' onClick={dec}>Remove</button>
       </div>

      <pre>{JSON.stringify(state, null, 2)}</pre>

    </div>
  );
}

export default App;
