import React, {useState, useEffect, useRef} from "react";

function App() { 
  const [value, setValue] = useState('')
  const renderCount = useRef(1)
  const inputRef = useRef(null)
  const prevValue = useRef('')

  const focus = () => inputRef.current.focus()

  useEffect(()=>{
      renderCount.current++
      console.log(inputRef.current.value);
  })

  useEffect(()=>{
    prevValue.current = value;
  })

  return (
    <div className='container'>
        <p>Amount of rendering: {renderCount.current}</p>
        <p>Last value: {prevValue.current}</p>
        <input 
        ref={inputRef}
        type="text" 
        onChange={e=>setValue(e.target.value)} 
        value={value}
        />
        <button className="success" onClick={focus}>
            Click
        </button>
    </div>
  );
}

export default App;
