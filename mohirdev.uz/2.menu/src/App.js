import React, { useEffect, useState } from 'react';
import data from './data'
import Categories  from './Categories';
import Menu from './Menu';

const allCategories =  ['All',...new Set(data.map((item)=>item.category)) ]

function App() {
  //TODO
  const [menuItems, setMenuItems] = useState(data)
  const [categories, setCategories] = useState(allCategories)

  const filterItems = (cat)=>{
    if(cat === 'All'){
      setMenuItems(data)
      return
    }

    const newItems = data.filter((item)=>item.category === cat)
    setMenuItems(newItems)
  }

  return (
    <main>
      <section className='menu section'>
        <div className='title'>
            <h2>Our Menu</h2>
        </div>
        <div className='underline'></div>
        <Categories filterItems={filterItems} categories={categories} />
        <Menu items={menuItems}/>
      </section>
    </main>
  );
}

export default App;
