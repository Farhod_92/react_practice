import React from 'react';

const Categories = ({filterItems, categories}) => {
  return <h2 className='btn-container'>
    {  categories.map((cat, index)=>{
      //console.log('cat='+cat);
       return <button 
       key={index} 
       className='filter-btn' 
       onClick={()=>filterItems(cat)}
       >
         {cat}
         </button>
      })
    }
  </h2>;
};

export default Categories;